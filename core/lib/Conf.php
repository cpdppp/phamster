<?php
namespace core\lib;

class Conf
{
	static $conf = array();
	/**
	 * 获取单项配置信息
	 * @param  [type] $name 配置项名称
	 * @param  [type] $file 配置文件名称
	 */
	public static function get($name, $file)
	{
		/**
		 * 1. 判断配置文件是否存在
		 * 2. 判断配置是否存在
		 * 3. 缓存配置
		 */
		if(isset(self::$conf[$file])) {
			return self::$conf[$file][$name];
		} else {
			$path = PHAMSTER.'\core'.'\config'.'\\'.$file.'.php';
			if(is_file($path)) {
				$conf = include $path;
				if(isset($conf[$name])) {
					self::$conf[$file] = $conf;
					return $conf[$name];
				} else {
					throw new \Exception('没有'.$conf[$name].'这个配置项');
				}
			} else {
				throw new \Exception('找不到配置文件:'.$file);
			}
		}
	}
	/**
	 * 获取整个配置文件
	 * @param  [type] $file 配置文件名称
	 */
	public static function all($file)
	{
		if(isset(self::$conf[$file])) {
			return self::$conf[$file];
		} else {
			$path = PHAMSTER.'\core'.'\config'.'\\'.$file.'.php';
			if(is_file($path)) {
				$conf = include $path;
				self::$conf[$file] = $conf;
				return $conf;
			} else {
				throw new \Exception('找不到配置文件:'.$file);
			}
		}
	} 
}