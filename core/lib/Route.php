<?php
namespace core\lib;
use \core\lib\Conf;
class Route
{
	public $ctrl;
	public $action;
	public function __construct()
	{
		/**
		 * 1.隐藏index.php
		 * 2.获取url参数部分
		 * 3.返回对应控制器和方法
		 */
		if(isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] != '/') {
			$path = $_SERVER['REDIRECT_URL'];
			$path_arr = explode('/', trim($path,'/'));
			if($path_arr[0]) {
				$this->ctrl = $path_arr[0];
				unset($path_arr[0]);
			}
			if($path_arr[1]) {
				$this->action = $path_arr[1];
				unset($path_arr[1]);
			} else {

				$this->action = Conf::get('ACTION','route');
			}
			//将url控制器及操作名以后的部分转化成$_GET参数
			$count = count($path_arr)+2;			
			//原生php会将'域名/'后面的所有部分视为一个$_GET参数,且该参数值为空,所以应重新设置$_GET参数
			unset($_GET);
			$_GET = [];

			for($i = 2; $i < $count; $i = $i+2) {
				if(isset($path_arr[$i+1])) {
					$_GET[$path_arr[$i]] = $path_arr[$i+1];
				}
			}
			//dump($_GET);
		} else {
			$this->ctrl = Conf::get('CTRL','route');
			$this->action = Conf::get('ACTION','route');
		}
	}
}