<?php
namespace core\lib;
use \core\lib\Conf;
class Model extends \PDO
{
	public function __construct()
	{
		$db = Conf::all('database');
		try { 
			parent::__construct($db['dsn'],$db['username'],$db['password']);
		} catch (\PDOException $e){
			dump($e->getMessage());
		}
	}
}