<?php
namespace core;

class Phamster 
{
	public static $classMap = array();
	public $assign;
	public static function run()
	{
		\core\lib\Log::init();
		$route = new \core\lib\Route();
		$ctrl_class = $route->ctrl;
		$action = $route->action;
		$ctrl_file = APP.'\ctrl'.'\\'.$ctrl_class.'Ctrl.php';
		$ctrl_class = MODULE.'\ctrl'.'\\'.$ctrl_class.'Ctrl';
		if(is_file($ctrl_file)) {
			include $ctrl_file;
			$ctrl = new $ctrl_class();
			$ctrl->$action();
		} else {
			throw new \Exception('找不到控制器'.$ctrl_class);
		}
	}
	/**
	 * 自动加载类库
	 * @param class 使用new关键字产生的对象名，该参数无需手动传递
	 */
	public static function load($class)
	{
		if(isset($classMap[$class])) {
			return true;
		} else {
			$class = str_replace('\\', '/', $class);
			$file = PHAMSTER.'/'.$class.'.php';
			if(is_file($file)) {
				include $file;
				self::$classMap[$class] = $class;
			} else {
				return false;
			}
		}
	}
	/**
	 * 视图:变量函数
	 * @param  [String] $name  控制器传来的变量名
	 * @param  [type] 	$value 控制器传来的变量值
	 * 
	 */
	public function assign($name, $value)
	{
		$this->assign[$name] = $value;
	}
	/**
	 * 视图:调用函数
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	public function display($file)
	{
		$file = APP.'\view'.'\\'.$file.'View.php';
		if(is_file($file)) {
			extract($this->assign); //extract函数将数组中的键值对转化成变量
			include $file;
		} else {
		}
	} 
}