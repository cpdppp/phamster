<?php
/**
 *  入口文件
 *  1.定义常量
 *  2.加载函数库
 *  3.启动框架
 *  4.测试钩子
 */
define('PHAMSTER',realpath('.\\'));
define('CORE', PHAMSTER.'\core');
define('APP',PHAMSTER.'\app');
define('MODULE','\app');
define('DEBUG',true);
if('DEBUG'){
	ini_set('display_error', 'On');
}else{
	ini_set('display_error', 'Off');
}

include CORE.'\common\function.php';
include CORE.'\Phamster.php';

spl_autoload_register('\core\Phamster::load');
\core\Phamster::run();
